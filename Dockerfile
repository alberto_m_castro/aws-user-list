# Use a Python container with the AWS SDK already installed
FROM jpbarto/boto3

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./app /app

# Run listusers.py when the container launches
CMD ["python", "listusers.py"]