## List Users Program

This is a simple program that lists the users of an AWS account. The requirements are a keypair for programatic access for a user with sufficient permissions to list users in IAM.

### Running the Python Script with default environment 

To execute the program to list users assuming that the environment for AWS is set properly (either with environment variables or by setting `~/.aws/credentials` and `~/.aws/config` ):

```bash-shell
$ python ./app/listusers.py 
```

### Running the Dockerized Python Script

First of all build the container:
```bash-shell
$ docker build -t listusers .
```
Then set the environment variables with the corresponding values

```bash-shell
$ export AWS_ACCESS_KEY_ID="yourkeyid"
$ export AWS_SECRET_ACCESS_KEY="yoursecretaccesskey"
$ export AWS_REGION="us-east-1"
```

Then run:
```bash-shell
$ docker run --env AWS_ACCESS_KEY_ID \
             --env AWS_SECRET_ACCESS_KEY \
             --env AWS_REGION listusers
```

### Running the Dockerized Python Script with keys provided as secrets

We will use `summon` to replace the secrets (https://github.com/cyberark/summon) and `keyring` to provide the secrets (https://github.com/conjurinc/summon-keyring).

First you need to store the secrets, in a Mac the `keyring` provider gets the information from the Mac Keychain:

```bash-shell
$ security add-generic-password -s "summon" -a "aws/iam/user/test/access_key_id" -w "yourkeyid"
$ security add-generic-password -s "summon" -a "aws/iam/user/test/secret_access_key" -w "yoursecretaccesskey"
```

After all is set run the container wrapped in `summon`:

```bash-shell
$ summon -p keyring.py docker run --env-file @SUMMONENVFILE listusers
```
